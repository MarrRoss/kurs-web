<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="/template/styles/index.css">
        <link rel="stylesheet" href="/template/styles/table.css">
    </head>

    <body>
        <input type="checkbox" id="drawer-toggle" name="drawer-toggle"/>
        <label for="drawer-toggle" id="drawer-toggle-label"></label>

        <header class='header'>
            <div class="header-content">
                <span>Admin </span>
            </div>
        </header>
        <nav id="drawer">
           <ul>
           <li><a href="/admin/product">Управление товарами</a></li>
                <li><a href="/admin/category">Управление категориями</a></li>
                <li><a href="/admin/shop/update">Управление Информацией о сайте</a></li>
                <li><a href="/"><i class="fa fa-sign-out"></i>На сайт</a></li>
           </ul>
        </nav>
<div class="container">
    <div class="row">
    <br/>
    <a href="/admin/category">Управление Категориями</a>
    <hr>
        <br/>
    <h4>Удалить категорию #<?php echo $id; ?></h4>


    <p>Вы действительно хотите удалить эту категорию?</p>

    <form method="post">
        <input type="submit" name="submit" value="Удалить" />
    </form>

    </div>
</div>

<footer>
    © 2023 MarCompany, Inc. All rights reserved.
</footer>

</body>
</html>