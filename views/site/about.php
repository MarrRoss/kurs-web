<!DOCTYPE html>
<html lang="RU">

    <head>
        <link rel="stylesheet" href="/template/styles/index.css">
        <title>About</title>
    </head>

    <body>
        <input type="checkbox" id="drawer-toggle" name="drawer-toggle"/>
        <label for="drawer-toggle" id="drawer-toggle-label"></label>

        <header class='header'>
            <div class="header-content">
                <span>Найди свой стиль! </span>
                <img src="/upload/images/products/shop1.jpg" alt="card__image" width="30" height="30">
            </div>
        </header>
        <nav id="drawer">
           <ul>
              <li><a href="/">Главная страница</a></li>
              <li><a href="/about/">Об авторе</a></li>
              <li><a href="/contacts/">О компании</a></li>
              <?php if ($user_status): ?>                                        
                <li  class="nav-item"><a href="/user/login/"><i class="fa fa-lock"></i> Вход</a></li>
            <?php else: ?>
                <li class="nav-item"><a href="/user/logout/"><i class="fa fa-unlock"></i> Выход</a></li>
            <?php endif; ?>
           </ul>
        </nav>

        <div class="container">
            <div class=creator>
                <?php echo $shop['contact']; ?>
            </div>
        </div>
        
        <section class="bg-white">
            <div class="max-w-screen-xl px-4 py-12 mx-auto space-y-8 overflow-hidden sm:px-6 lg:px-8">
                <nav class="flex flex-wrap justify-center -mx-5 -my-2">
                    <div class="px-5 py-2">
                        Email: mar@mail.com
                    </div>
                </nav>
                    © 2023 MarCompany, Inc. All rights reserved.
            </div>
        </section>
    </body>
</html>