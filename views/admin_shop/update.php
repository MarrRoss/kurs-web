<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="/template/styles/index.css">
        <link rel="stylesheet" href="/template/styles/index.css">
    </head>

    <body>
        <input type="checkbox" id="drawer-toggle" name="drawer-toggle"/>
        <label for="drawer-toggle" id="drawer-toggle-label"></label>

        <header class='header'>
            <div class="header-content">
                <span>Admin </span>
            </div>
        </header>
        <nav id="drawer">
           <ul>
           <li><a href="/admin/product">Управление товарами</a></li>
                <li><a href="/admin/category">Управление категориями</a></li>
                <li><a href="/admin/shop/update">Управление Информацией о сайте</a></li>
                <li><a href="/"><i class="fa fa-sign-out"></i>На сайт</a></li>
           </ul>
        </nav>
<section>
    <div class="container">
        <div class="row">

            <br/>
            <!-- <a href="/admin/product">Управление Информацией о сайте</a> -->


            <h4>Редактирование</h4>

            <br/>

            <div class="col-lg-4">
                <div class="login-form">
                    <form action="#" method="post" enctype="multipart/form-data">

                        <p>Основная информация сайта</p>
                        <label>
                            <input type="text" name="general" placeholder="" value="<?php echo $shop['general']; ?>">
                        </label><br/>

                        <p>О сайте</p>
                        <label>
                            <input type="text" name="about" placeholder="" value="<?php echo $shop['about']; ?>">
                        </label><br/>

                        <p>Контакты</p>
                        <label>
                            <input type="text" name="contact" placeholder="" value="<?php echo $shop['contact']; ?>">
                        </label>

                        <br/><br/>
                        
                        <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
                        
                        <br/><br/>
                        
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

<footer>
    © 2023 MarCompany, Inc. All rights reserved.
</footer>

</body>
</html>