<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/template/styles/index.css">
</head>

<body>
        <input type="checkbox" id="drawer-toggle" name="drawer-toggle"/>
        <label for="drawer-toggle" id="drawer-toggle-label"></label>

        <header class='header'>
            <div class="header-content">
                <span>Найди свой стиль!</span>
                <img src="/upload/images/products/shop1.jpg" alt="card__image" width="30" height="30">
            </div>
        </header>
        <nav id="drawer">
           <ul>
              <li><a href="/">Главная страница</a></li>
              <li><a href="/about/">Об авторе</a></li>
              <li><a href="/contacts/">О компании</a></li>
              <?php if ($user_status): ?>                                        
                <li  class="nav-item"><a href="/user/login/"><i class="fa fa-lock"></i> Вход</a></li>
            <?php else: ?>
                <li class="nav-item"><a href="/user/logout/"><i class="fa fa-unlock"></i> Выход</a></li>
            <?php endif; ?>
           </ul>
        </nav>

        <div class="container">
        <section class="bg-white">
                <?php if ($product['html']): ?>  
                    <?php echo $product['html']; ?>
                    <?php else: ?>
                        <img src="<?php echo $image; ?>" height=400 width=400>
                        <br/>
                        <h2><?php echo $product['name']; ?></h2>
                        <br/>
                        <p>Код товара: <?php echo $product['code']; ?></p>
                        <span>
                            <span>RUB <?php echo $product['price']; ?></span>
                        </span>
                        <p>Производитель: <?php echo $product['brand']; ?></p>
                        <br/>
                        <h3>Описание товара:</h3> <?php echo $product['description']; ?>
                <?php endif; ?>
                </section>
        </div>
    </body>
</html>